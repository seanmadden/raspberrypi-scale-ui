export enum UnitOfMeasurement {
  gram,
  kilogram,
  ounce,
}
