import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ScaleService } from "../service/scale.service";
import { HttpModule } from "@angular/http";
import { NotnegativePipe } from "../pipes/notnegative.pipe";
import { IngredientService } from "../service/ingredient.service";
import { UnitPipe } from "../pipes/unit.pipe";
import { RoundPipe } from "../pipes/round.pipe";
import { RecipeService } from "../service/recipe.service";
import { SearchComponent } from "./nutritionix/search.component";
import { FormsModule } from "@angular/forms";
import { NutritionixService } from "./nutritionix/nutritionix.service";
import { SearchResultComponent } from "./nutritionix/search-result.component";
import { RouterModule, Routes } from "@angular/router";
import { RecipeBuilderComponent } from "./recipe-builder/recipe-builder.component";
import { ScaleComponent } from "./scale/scale.component";
import { ItemComponent } from "./nutritionix/item.component";

const appRoutes: Routes = [
  { path: '',
    redirectTo: '/scale',
    pathMatch: 'full'
  },
  {
    path: 'scale',
    component: ScaleComponent
  },
  {
    path: 'recipe-builder',
    component: RecipeBuilderComponent
  }
];


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    SearchResultComponent,
    RecipeBuilderComponent,
    ScaleComponent,
    ItemComponent,
    NotnegativePipe,
    UnitPipe,
    RoundPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [
    ScaleService,
    IngredientService,
    RecipeService,
    NutritionixService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
