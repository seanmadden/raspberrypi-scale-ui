import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import 'rxjs/add/operator/toPromise';
import * as io from "socket.io-client";


@Injectable()
export class ScaleService {
  private socket: io;

  constructor(private http: Http) {
  }

  create(address: String): void {
    if (!this.socket) {
      this.socket = io(address);
    }
  }

  addEventHandler(event: String, handler: Function): void {
    if (this.socket) {
      this.socket.on(event, handler);
    }
  }

  emit(message: String, data: any): void {
    if (data == null) {
      this.socket.emit(message)
    } else {
      this.socket.emit(message, data)
    }
  }

  getWeight(): Promise<Number> {
    return this.http.get("http://192.168.1.235:5050/weight")
      .toPromise()
      .then(response => response.json())
  }

  tare(): void {
    this.socket.emit('tare');
    // console.log("tare!");
    // this.http.get("http://192.168.1.235:5050/tare")
    //   .toPromise()
    //   .then(response => console.log(response));
  }
}
