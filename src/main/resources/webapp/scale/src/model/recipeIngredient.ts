import {Ingredient} from "./ingredient";
import {UnitOfMeasurement} from "./unitOfMeasurement";

export class RecipeIngredient {
  ingredient: Ingredient;
  amount: Number;
  measurement: UnitOfMeasurement;
}
