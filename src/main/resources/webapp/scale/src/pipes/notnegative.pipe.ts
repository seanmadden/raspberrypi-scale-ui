import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'notnegative'
})
export class NotnegativePipe implements PipeTransform {
  transform(value: number): string {
    if (value < 0) {
      return "0 g"
    } else {
      return `${value} g`
    }
  }
}
