import { Pipe, PipeTransform } from '@angular/core';
import {UnitOfMeasurement} from "../model/unitOfMeasurement";

@Pipe({
  name: 'unit'
})
export class UnitPipe implements PipeTransform {
  transform(value: UnitOfMeasurement): string {
    if (value === UnitOfMeasurement.gram) {
      return 'g'
    }
  }
}
