import { Component } from "@angular/core";
import { NutritionixService } from "./nutritionix.service";
import { CommonSearchResult } from "../../model/common-search-result.model";
import { BrandedSearchResult } from "../../model/branded-search-result.model";

@Component({
  selector: 'nx-search',
  templateUrl: './search.component.html'
})
export class SearchComponent {
  searchTerm: any;
  commonSearchResults: CommonSearchResult[];
  brandedSearchResults: BrandedSearchResult[];

  constructor(private nutritionixService: NutritionixService) {
  }

  searchFood() {
    this.nutritionixService.search(this.searchTerm)
      .subscribe((data) => {
        console.log(data);
        this.commonSearchResults = data.json()['common'];
        this.brandedSearchResults = data.json()['branded'];
        console.log(this.brandedSearchResults);
      });
  }
}
