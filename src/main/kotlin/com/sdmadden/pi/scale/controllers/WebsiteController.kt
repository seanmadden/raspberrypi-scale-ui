package piscale

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class WebsiteController {

    @RequestMapping("/")
    fun index() : String {
        return "index"
    }
}