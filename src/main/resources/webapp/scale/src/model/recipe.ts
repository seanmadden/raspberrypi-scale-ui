import {RecipeIngredient} from "./recipeIngredient";

export class Recipe {
  name: String;
  ingredients: RecipeIngredient[]
}
