import {UnitOfMeasurement} from './unitOfMeasurement'

export class Ingredient {
  name: String;
  servingSize: number;
  unitOfMeasurement: UnitOfMeasurement;
  calories: number;
  fat: number;
  carbohydrates: number;
  protein: number;
}
