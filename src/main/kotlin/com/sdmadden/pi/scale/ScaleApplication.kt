package piscale

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class ScaleApplication

fun main(args: Array<String>) {
    SpringApplication.run(ScaleApplication::class.java, *args)
}
