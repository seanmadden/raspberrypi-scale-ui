import { Component, Input, OnInit } from '@angular/core';
import { ScaleService } from "../../service/scale.service";
import { IngredientService } from "../../service/ingredient.service";
import { Ingredient } from "../../model/ingredient";
import { RecipeService } from "../../service/recipe.service";
import { Recipe } from "../../model/recipe";
import { RecipeIngredient } from "../../model/recipeIngredient";
import { UnitOfMeasurement } from "../../model/unitOfMeasurement";

@Component({
  selector: 'scale',
  templateUrl: './scale.component.html',
  styleUrls: ['./scale.component.css']
})
export class ScaleComponent implements OnInit {
  weight: number;
  @Input() targetWeight: any;
  theColor: string = '';
  targetIngredient: RecipeIngredient;
  targetRecipe: Recipe;
  usedIngredients: RecipeIngredient[];
  unit: string;
  totalCalories = 0;
  totalCarbohydrates = 0;
  totalFat = 0;
  totalProtein = 0;

  ngOnInit(): void {
    this.scaleService.create('localhost:5050');
    // this.scaleService.create('192.168.1.235:5050');

    this.scaleService.addEventHandler('connect', () => {
      console.log('connected via socket service');
    });

    this.scaleService.addEventHandler('weight_update', (data) => {
      this.weight = data;
      this.theColor = this.getFontColor();
    });

    setInterval(() => {
      this.scaleService.emit('weight', null)
    }, 250);

    this.targetRecipe = this.ingredientService.getSourdoughRecipe();

    this.targetIngredient = this.targetRecipe.ingredients.pop();
    this.unit = UnitOfMeasurement[this.targetIngredient.measurement];
    this.targetWeight = this.targetIngredient.amount;

    this.recipeService.getIngredients().subscribe((change) => {
      this.usedIngredients = change;
    });
  }

  tare(): void {
    this.scaleService.tare();
  }

  nextIngredient(): void {
    const weighedIngredient: RecipeIngredient = new RecipeIngredient();
    weighedIngredient.ingredient = new Ingredient();
    weighedIngredient.ingredient.carbohydrates = ((this.targetIngredient.ingredient.carbohydrates * this.weight) / this.targetIngredient.ingredient.servingSize);
    weighedIngredient.ingredient.fat = ((this.targetIngredient.ingredient.fat * this.weight) / this.targetIngredient.ingredient.servingSize);
    weighedIngredient.ingredient.protein = ((this.targetIngredient.ingredient.protein * this.weight) / this.targetIngredient.ingredient.servingSize);
    weighedIngredient.ingredient.calories = ((this.targetIngredient.ingredient.calories * this.weight) / this.targetIngredient.ingredient.servingSize);

    weighedIngredient.ingredient.name = this.targetIngredient.ingredient.name;
    weighedIngredient.ingredient.unitOfMeasurement = this.targetIngredient.ingredient.unitOfMeasurement;

    weighedIngredient.amount = this.weight;
    weighedIngredient.measurement = this.targetIngredient.measurement;

    this.recipeService.addIngredient(weighedIngredient);
    this.targetIngredient = this.targetRecipe.ingredients.pop();
    if (this.targetIngredient) {
      this.unit = UnitOfMeasurement[this.targetIngredient.measurement];
      this.targetWeight = this.targetIngredient.amount;
    }

    this.totalCalories += weighedIngredient.ingredient.calories;
    this.totalFat += weighedIngredient.ingredient.fat;
    this.totalCarbohydrates += weighedIngredient.ingredient.carbohydrates;
    this.totalProtein += weighedIngredient.ingredient.protein;

  }

  setWeight(): void {
    this.scaleService.emit("set_weight", this.targetWeight)
  }

  updateTargetWeight(inputValue: Event) {
    this.targetWeight = inputValue;
    this.theColor = this.getFontColor();
  }

  getFontColor() {
    const weight = Number(this.weight);
    const targetWeight = Number(this.targetWeight);
    if (weight > targetWeight) {
      return 'red';
    } else if (weight === targetWeight) {
      return 'green';
    } else {
      return '';
    }
  }

  constructor(private scaleService: ScaleService,
              private ingredientService: IngredientService,
              private recipeService: RecipeService) {
    this.weight = 0;
  }

}
