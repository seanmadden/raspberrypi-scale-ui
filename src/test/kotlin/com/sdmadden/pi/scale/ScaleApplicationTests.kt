package com.sdmadden.pi.scale

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class ScaleApplicationTests {

	@Test
	fun contextLoads() {
	}

}
