
import { Component, Input } from "@angular/core";
import { BrandedSearchResult } from "../../model/branded-search-result.model";

@Component({
  templateUrl: './item.component.html',
  selector: 'nx-item'
})
export class ItemComponent {
  constructor() {}

  @Input() item: BrandedSearchResult;
}
