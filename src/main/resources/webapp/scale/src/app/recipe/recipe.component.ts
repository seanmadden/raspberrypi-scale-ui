import {Component, OnInit} from "@angular/core";
import {Recipe} from "../../model/recipe";
import {IngredientService} from "../../service/ingredient.service";

@Component({
  selector: 'recipe-view',
  templateUrl: './recipe.component.html'
})
export class RecipeComponent implements OnInit{
  recipe: Recipe;

  constructor(
    private ingredientService: IngredientService
  ) {}

  ngOnInit(): void {
    this.recipe = this.ingredientService.getSourdoughRecipe();
  }
}
