
import { SearchResult } from "./search-result.model";

export class BrandedSearchResult extends SearchResult {
  nix_brand_id: string;
  brand_name_item_name: string;
  nf_calories: string;
  photo: {
    thumb: string,
    highres?: string
  };
  brand_name: string;
  region: string;
  brand_type: string;
  nix_item_id: string;
}
