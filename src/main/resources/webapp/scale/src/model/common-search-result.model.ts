
import { SearchResult } from "./search-result.model";

export class CommonSearchResult extends SearchResult {
  tag_id: string;
}
