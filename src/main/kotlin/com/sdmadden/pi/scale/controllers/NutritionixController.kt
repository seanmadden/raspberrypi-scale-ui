package piscale

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelManager
import org.springframework.web.bind.annotation.*

@RestController
class NutritionixController() {

    @CrossOrigin()
    @RequestMapping(path = arrayOf("/food/search"), method = arrayOf(RequestMethod.GET))
    fun search(@RequestParam(name = "q") query: String): String {
        val params = listOf(Pair("query", query))

        val (request, response, result) =
                Fuel.get("https://trackapi.nutritionix.com/v2/search/instant", params)
                .responseString()

        return result.component1()!!
    }

    @CrossOrigin()
    @RequestMapping(path = arrayOf("/food/{id}"), method = arrayOf(RequestMethod.GET))
    fun getById(@PathVariable id: String): String {
        val params = listOf(Pair("nix_item_id", id))

        val (request, response, result) =
                Fuel.get("https://trackapi.nutritionix.com/v2/search/item", params)
                .responseString()

        return result.component1()!!
    }

    init {
        FuelManager.instance.baseHeaders = mapOf(
                "x-app-id" to "2e945632",
                "x-app-key" to "b6d7cc86507e311ed89c93cc00c1ece3",
                "x-remote-user-id" to "0")
    }
}