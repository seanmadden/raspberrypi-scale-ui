
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from "@angular/core";
import { BrandedSearchResult } from "../../model/branded-search-result.model";
import { NutritionixService } from "./nutritionix.service";

@Component({
  templateUrl: './search-result.component.html',
  selector: 'nx-search-result',
  styleUrls: ['./search-result.style.scss']
})
export class SearchResultComponent implements OnChanges {
  @Input() brandedResults: BrandedSearchResult[];
  @Output('selected') resultSelected = new EventEmitter<any>();

  constructor(
    private nutritionixService: NutritionixService
  ) {}

  onClick(id) {
    this.resultSelected.emit("hello world");
    this.nutritionixService.getItemById(id)
      .subscribe((response) => {
          console.log(response.json());
          // this.resultSelected.emit(response.json())
        // this.resultSelected.emit("hello world");
      });
  }

  ngOnChanges(changes: SimpleChanges) {
  }
}
