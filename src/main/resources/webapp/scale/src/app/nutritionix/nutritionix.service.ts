import { Injectable } from "@angular/core";
import { Http, RequestOptions, URLSearchParams } from "@angular/http";

@Injectable()
export class NutritionixService {
  url = "http://localhost:8080/food";
  constructor(private http: Http) {
  }

  search(term) {
    const options = new RequestOptions();
    const params = new URLSearchParams();
    params.append('q', term);
    options.params = params;
    return this.http.get(`${this.url}/search`, options)
  }

  getItemById(id) {
    return this.http.get(`${this.url}/${id}`)
  }
}
