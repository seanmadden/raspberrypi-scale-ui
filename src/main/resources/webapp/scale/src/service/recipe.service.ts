import { Injectable } from "@angular/core";
import { UnitOfMeasurement } from "../model/unitOfMeasurement";
import { Recipe } from "../model/recipe";
import { IngredientService } from "./ingredient.service";
import { Ingredient } from "../model/ingredient";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { RecipeIngredient } from "../model/recipeIngredient";

@Injectable()
export class RecipeService {
  private ingredients: BehaviorSubject<RecipeIngredient[]> = new BehaviorSubject([]);

  constructor(private ingredientService: IngredientService) {
  }

  addIngredient(ingredient: RecipeIngredient) {
    this.ingredients.getValue().push(ingredient);
    this.ingredients.next(this.ingredients.getValue());
  }

  getIngredients(): Observable<RecipeIngredient[]> {
    return this.ingredients.asObservable();
  }

}
