import {Injectable} from "@angular/core";
import {Ingredient} from "../model/ingredient";
import {UnitOfMeasurement} from "../model/unitOfMeasurement";
import {Recipe} from "../model/recipe";

@Injectable()
export class IngredientService {
  readonly water: Ingredient = {
    name: 'Water',
    servingSize: 1,
    unitOfMeasurement: UnitOfMeasurement.gram,
    calories: 0,
    fat: 0,
    protein: 0,
    carbohydrates: 0
  };
  readonly breadFlour: Ingredient = {
    name: 'Bread Flour',
    servingSize: 30,
    unitOfMeasurement: UnitOfMeasurement.gram,
    calories: 110,
    fat: 0,
    protein: 4,
    carbohydrates: 22
  };
  readonly avocadoOil: Ingredient = {
    name: 'Avocado Oil',
    servingSize: 14,
    unitOfMeasurement: UnitOfMeasurement.gram,
    calories: 130,
    fat: 14,
    protein: 0,
    carbohydrates: 0
  };
  readonly salt: Ingredient = {
    name: 'Sea Salt',
    servingSize: 1,
    unitOfMeasurement: UnitOfMeasurement.gram,
    calories: 0,
    fat: 0,
    protein: 0,
    carbohydrates: 0
  };
  readonly starter: Ingredient = {
    name: 'Sourdough Starter',
    servingSize: 30,
    unitOfMeasurement: UnitOfMeasurement.gram,
    calories: 55,
    fat: 0,
    protein: 4,
    carbohydrates: 23

  };

  readonly sourdoughBread: Recipe = {
    name: 'Sourdough Bread',
    ingredients: [
      {
        ingredient: this.starter,
        measurement: UnitOfMeasurement.gram,
        amount: 150
      },
      {
        ingredient: this.breadFlour,
        measurement: UnitOfMeasurement.gram,
        amount: 500
      },
      {
        ingredient: this.water,
        measurement: UnitOfMeasurement.gram,
        amount: 250
      },
      {
        ingredient: this.avocadoOil,
        measurement: UnitOfMeasurement.gram,
        amount: 25
      },
      {
        ingredient: this.salt,
        measurement: UnitOfMeasurement.gram,
        amount: 10
      }

    ]
  };

  getSourdoughRecipe() {
    return this.sourdoughBread;
  }

  getBreadFlour() {
    return this.breadFlour;
  }
}
