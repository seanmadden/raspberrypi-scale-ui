
export class SearchResult {
  food_name: string;
  serving_unit: string;
  serving_qty: 1;
  photo: {
    thumb: string
  };
}
